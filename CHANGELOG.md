## [0.7.1]
- Fixed broken mongodb getting item through id

## [0.7.0]
- Introducing pagination 
  - breaking change as structure of results is 1 level deeper under 'items'

## [0.6.1]
- Ability to change Log level through `LOG_LEVEL` environment variable

## [0.6.0]
- Add prometheus metrics accessed at `metrics` endpoint

## [0.5.1]
- Fix swagger link when running exe

## [0.5.0]
- Set default env to be production as idea is that this is run as a complete unit in itself

## [0.4.2]
- Fix logging for POST

## [0.4.1]
- Make logging cleaner with helper and filters
- New ENV variable `LOG` which if set to `structured` gives logs in JSON structure

## [0.4.0]
- Use `/swagger` for API docs as API calls be made from it
- Add concept of correlation id to POST

## [0.3.4]
- Use logger for all methods
- Fix tapioca 'type' error

## [0.3.3]
- Improve logging
- Fix GET query syntax for in memory mode

## [0.3.2]
- Add options endpoint with allowed verbs

## [0.3.1]
- Add 'rackup' gem dependency needed in later versions of code

## [0.3.0]
- Proper response for showing all of an entity with in memory
- Allow taking in id as a parameted
- Allow seeding db from 'db.json'

## [0.2.2]
- Basic docs endpoint at '/docs'

## [0.2.1]
- Cleaner update of Mongodb data

## [0.2.0]
- With ability to connect to MongoDb

## [0.1.0] - 2021-12-10

- Initial release
