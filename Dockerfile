FROM ruby:3.3-alpine

RUN apk add --update build-base
RUN addgroup -S appgroup && adduser -S app -G appgroup

RUN chmod -R 777 /usr/local/bundle

USER app

EXPOSE 4567

RUN mkdir /home/app/service
WORKDIR /home/app/service
COPY . /home/app/service
RUN gem install bundler
RUN bundle install
RUN mkdir /home/app/serve
WORKDIR /home/app/serve

ENTRYPOINT ["ruby", "/home/app/service/exe/schemaless_rest_api"]
CMD []
