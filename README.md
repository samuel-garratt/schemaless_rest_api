# SchemalessRestApi

Simple schema less rest API that can be run in docker and have entities configured through a simple list of environment variables.
It follows convention over configuration to allow you to get a REST api that stores unstructured data fast.

## Installation

Pull the docker image with 
```sh
docker pull registry.gitlab.com/demo-pocs/schemaless_rest_api
```

or install through ruby with

```sh
gem install schemaless_rest_api
```

## Usage

Run this in docker with

```sh
docker run -e models="['Contact', 'Case']" -p 4567:4567 --rm -it registry.gitlab.com/demo-pocs/schemaless_rest_api
```

You can also create a file called `db.json` with the structure:
```json
{
  "case": [],
  "contact": [
    {
        "firstName": "Test",
        "lastName": "Last"
    }
  ]
}
```
to setup a 'case' and 'contact' model and initialise 1 contact. Make sure to start a volume to `/home/app/serve` to pass this db.json into the container. 

E.g

```
docker run -v $(pwd):/home/app/serve -p 4567:4567 --rm -it registry.gitlab.com/demo-pocs/schemaless_rest_api
```

> Note the initialisation of data only works for in memory (not with Mongo db)

This can connect to mongodb with `mongodb` env variable, where `mongodb` is
the host location of the mongodb.

One can spin up a mongo database in docker with the command

```
docker run --rm --name mongodb -p 27017:27017 -it mongo:latest
```

Then set the environment variable to this `export mongodb=localhost` and start this service again.

Stop with `docker stop mongodb`

Use `base_path` environment variable to host the application at a subpath and have Swagger doc point to the correct place.

### Pagination

Results are paginated. Use the following to get paginated results

`/model?page_size=1&page=1`

where `page` is the page number and `page_size` is the number of results.

### Logging
If you are running this with multiple instances in an environment such as Kubernetes set the logging to structured by setting `LOG` environment variable to `structured`.

Use `LOG_LEVEL` environment variable to change the log level, default being `INFO`. 

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/schemaless_rest_api. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://github.com/[USERNAME]/schemaless_rest_api/blob/master/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the SchemalessRestApi project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/schemaless_rest_api/blob/master/CODE_OF_CONDUCT.md).
