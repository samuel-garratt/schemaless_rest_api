# frozen_string_literal: true

# typed: true

require_relative "schemaless_rest_api/version"
require_relative "schemaless_rest_api/entities"
require_relative "schemaless_rest_api/swagger_builder"
require_relative "schemaless_rest_api/seed"
require "tapioca"
require "logger"
require "ougai"
require "json"
require "securerandom"

ENV["APP_ENV"] ||= "production"
ENV["base_path"] ||= ""

# Global params for Schemalass REST API
module SchemalessRestApi
  if ENV["Log"] == "structured"
    @logger = Ougai::Logger.new($stdout)
    @log_type = :ougai
  else
    @logger = Logger.new($stdout)
    @log_type = :basic
  end
  LOGLEVELS = %w[DEBUG INFO WARN ERROR FATAL UNKNOWN].freeze
  log_level ||= LOGLEVELS.index ENV.fetch("LOG_LEVEL", "INFO")
  @logger.level = log_level

  class << self
    # @return Logger
    attr_accessor :logger

    # @return Logger type
    attr_accessor :log_type
  end

  class Error < StandardError; end
end

def models_env_not_set
  !ENV["models"] || T.must(ENV["models"]).empty?
end

def extract_models
  return [] if !Entities.models.empty? && models_env_not_set

  error_msg = "Make 'models' environment variable an array (e.g ['model1', 'model2'])"
  raise "Please set 'models' ENV variable or create 'db.json'. #{error_msg}" if ENV["models"].nil?

  models = eval(T.must(ENV["models"]).delete_prefix('"').delete_suffix('"')) # rubocop:disable Security/Eval
  raise error_msg unless models.is_a? Array

  models
end

SEED_FILE = "db.json"

init_based_on_seed if File.exist? SEED_FILE

extract_models.each do |model|
  Entities.models[model.to_sym] = {}
end

puts "Modelling #{Entities.models.keys}"

if ENV["mongodb"]
  require_relative "schemaless_rest_api/mongo_client"
else
  puts "[INFO] Using in memory storage. Pass in 'mongodb' ENV variable to store
in db"
end
require_relative "schemaless_rest_api/rest_server"
