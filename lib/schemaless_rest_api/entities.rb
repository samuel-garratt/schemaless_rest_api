# frozen_string_literal: true

# typed: true

# Entities mapped by environment variables
class Entities
  @models = {}
  class << self
    # @return [Hash] Hash of models
    attr_accessor :models

    def page_data(model, params, values)
      page = params[:page].to_i.positive? ? params[:page].to_i : 1
      page_size = params[:page_size].to_i.positive? ? params[:page_size].to_i : 10
      total_count = values.count
      total_pages = (total_count / page_size.to_f).ceil
      page_res = {
        current_page: page,
        page_size: page_size,
        total_count: total_count,
        total_pages: total_pages,
        _links: {
          self: { href: "/#{model}?page=#{page}&page_size=#{page_size}" }
        }
      }
      page_res[:_links][:next] = { href: "/#{model}?page=#{page + 1}&page_size=#{page_size}" } if page < total_pages
      page_res[:_links][:prev] = { href: "/#{model}?page=#{page - 1}&page_size=#{page_size}" } if page > 1
      page_res
    end

    def query_from_params(params)
      query = params.dup
      query.delete(:page)
      query.delete(:page_size)
      query
    end

    # Find all values for given model querying via params
    def find_all(model, params)
      query = query_from_params params
      total_items = if query == {}
                      Entities.models[model].values
                    else
                      Entities.models[model].values.find_all do |val|
                        val[query.keys[0].to_s].to_s == query.values[0]
                      end
                    end
      pagination = page_data(model, params, total_items)
      skip = (pagination[:current_page] - 1) * pagination[:page_size]

      items = total_items.drop(skip).take(pagination[:page_size])
      response = {
        _embedded: {},
        pagination: pagination
      }
      response[:_embedded][model.to_sym] = items
      response
    end
  end
end
