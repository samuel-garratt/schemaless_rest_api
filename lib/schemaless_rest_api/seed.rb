# frozen_string_literal: true

# typed: true

def init_based_on_seed
  return if ENV["mongodb"]

  puts "Seeding db based on '#{SEED_FILE}'"
  seed_data = JSON.parse(File.read(SEED_FILE))
  seed_data.each do |entity, values|
    Entities.models[entity.to_sym] = {} unless Entities.models[entity.to_sym]
    seed_in_memory_data(entity, values)
  end
end

def seed_in_memory_data(entity, values)
  unless values.is_a? Array
    SchemalessRestApi.logger.warning "Entity '#{entity}' doesn't have array, skipping"
    return
  end

  values.each do |value|
    next unless value["id"]

    Entities.models[entity.to_sym][value["id"].to_s] = value
  end
end
