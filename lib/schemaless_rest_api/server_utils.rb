# frozen_string_literal: true

# typed: false

# Helper methods added to RestServer
module ServerUtils
  def log(messages)
    if SchemalessRestApi.log_type == :ougai
      log_structured(messages)
    else
      message = messages.values.join(", ")
      SchemalessRestApi.logger.info "#{message}, #{request.request_method} #{request.fullpath}, CorrelationId: #{@request_id}"
    end
  end

  def id?(model, id)
    Entities.models[model].key?(id)
  end

  def not_have(id)
    [404, JSON.generate({ error: "'#{id}' not found" })]
  end

  def log_structured(messages)
    log_msg = {
      method: request.request_method,
      path: request.fullpath,
      correlationId: @request_id
    }
    messages.each do |key, value|
      log_msg[key] = value
    end
    SchemalessRestApi.logger.info(log_msg)
  end

  private :log_structured
end
