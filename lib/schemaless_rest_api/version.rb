# frozen_string_literal: true

# typed: true

module SchemalessRestApi
  VERSION = "0.7.1"
end
