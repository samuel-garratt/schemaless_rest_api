# frozen_string_literal: true

# typed: true

require_relative "lib/schemaless_rest_api/version"

Gem::Specification.new do |spec|
  spec.name = "schemaless_rest_api"
  spec.version = SchemalessRestApi::VERSION
  spec.authors = ["Samuel Garratt"]
  spec.email = ["samuel.garratt@sentify.co"]

  spec.summary = "TSimple schema less rest API that have entities configured through environment variables"
  spec.description = "Simple schema less rest API that can be run in docker and have entities configured through a simple list of environment variables.
It follows convention over configuration to allow you to get a REST api that stores unstructured data fast."
  spec.homepage = "https://gitlab.com/samuel-garratt/schemaless_rest_api"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/samuel-garratt/schemaless_rest_api"
  spec.metadata["changelog_uri"] = "https://gitlab.com/samuel-garratt/schemaless_rest_api/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir.glob("{lib,exe}/**/*")
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "mongo"
  spec.add_dependency "ougai"
  spec.add_dependency "prometheus-client"
  spec.add_dependency "puma"
  spec.add_dependency "rackup"
  spec.add_dependency "route_downcaser"
  spec.add_dependency "sinatra"
  spec.add_dependency "thor"
  # For more information and examples about making a new gem, checkout our
  # guide at: https://bundler.io/guides/creating_gem.html
end
