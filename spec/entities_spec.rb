# frozen_string_literal: true

# typed: true

RSpec.describe Entities do
  T.bind(self, T.untyped)
  it "returns a value with no query" do
    result = Entities.find_all(:locations, {})[:_embedded][:locations]
    expect(result).to be_instance_of Array
    expect(result.length).to eq 2
  end

  it "returns value from query" do
    result = Entities.find_all(:locations, { city: "Chicago" })[:_embedded][:locations]
    expect(result.length).to eq 1
  end

  it "returns empty for no value" do
    result = Entities.find_all(:locations, { city: "Wellington" })[:_embedded][:locations]
    expect(result.length).to eq 0
  end
end
