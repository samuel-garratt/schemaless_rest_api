# frozen_string_literal: true

# typed: true

RSpec.describe SchemalessRestApi do
  T.bind(self, T.untyped)
  it "extracts models from environment variable" do
    expect(extract_models).to eq %w[case contact]
  end

  it "has a logger" do
    expect(SchemalessRestApi.logger).to be_instance_of Logger
  end

  it "can build Swagger doc for models" do
    doc = JSON.parse SwaggerBuilder.build_swagger_for(Entities.models, "localhost:4567")
    expect(doc["paths"].length).to eq 7
    expect(doc["paths"]["/"].length).to eq 1
  end
end
