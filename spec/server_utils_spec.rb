# frozen_string_literal: true

# typed: true

RSpec.describe ServerUtils do
  T.bind(self, T.untyped)
  before do
    extend ServerUtils
  end

  it "explains a not found resource" do
    test_id = "5"
    response = not_have(test_id)
    expect(response[0]).to eq(404)
    expect(response[1]).to include("'#{test_id}' not found")
  end
end
